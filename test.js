var Memcached = require("memcached"),
	express = require("express"),
	http = require("http"),
	https = require("https"),
	async = require("async");

GLOBAL.memcached = new Memcached("localhost:11211");
var app = express();
app.get("/city/:city/radius/:radius/limit/:limit/events.:format(json)", function(request, response){
	getEvents(request, response, function(events){
		var calls = [], result = [];
		for(var i = 0; i < events.length && i < Number(request.params.limit); i++){

			var artists = new Array();
			for(var y = 0; y < events[i].artists.length; y++){
				var a = {
					name: events[i].artists[y].name
				};
				artists.push(a);
				addA(a);
			}

			var e = {
				eventInfo: {
					datetime: events[i].datetime,
					ticket_status: events[i].ticket_status,
					city: events[i].venue.city,
					name: events[i].venue.name,
					latitude: events[i].venue.latitude,
					longitude: events[i].venue.longitude
				},
				artist: artists
			}
			result.push(e);
			addE(e);
		}
		async.parallel(calls, function(error, results){
			response.writeHead(200, { "Content-Type": "application/json" });
			response.end(new Buffer(JSON.stringify(result), "utf8"));
		});

		function addE(e){
			calls.push(function(callback){ getEventWeather(e, callback) });
		}
		function addA(a){
			calls.push(function(callback){ getEventArtist(a, callback) });
		}
	});
});
app.listen(3000);
function getEvents(request, response, callback){
	memcached.get("api.bandsintown.com:" + request.params.city + "_" + request.params.radius, function(error, data){
		if(!data){
			var options = {
				host: "api.bandsintown.com",
				port: "80",
				path: "/events/search?location=" + request.params.city + "&radius=" + request.params.radius + "&format=json",
				method: "get"
			};
			var req = http.request(options, function(res){
				var chunk = "";
				res.on("data", function(data){
					chunk += data;
				});
				res.on("end", function(){
					memcached.set("api.bandsintown.com:" + request.params.city + "_" + request.params.radiusm, JSON.parse(chunk), 24 * 60 * 60 * 1000, function(error){
						if(error) console.log(error);

						callback(JSON.parse(chunk));
					});
				});
			});
			req.end();
		}else
			callback(data);
	});
}
function getEventWeather(event, callback){
	memcached.get("api.openweathermap.com:" + String(event.eventInfo.latitude).split('.')[0] + "_" + String(event.eventInfo.longitude).split('.')[0], function(error, data){
		if(!data){
			var options = {
				host: "api.openweathermap.org",
				port: "80",
				path: "/data/2.5/forecast?lat=" + String(event.eventInfo.latitude).split('.')[0] + "&lon=" + String(event.eventInfo.longitude).split('.')[0] + "&units=metric",
				method: "get"
			};
			var req = http.request(options, function(res){
				var chunk = "";
				res.on("data", function(data){
					chunk += data;
				});
				res.on("end", function(){
					memcached.set("api.openweathermap.com:" + String(event.eventInfo.latitude).split('.')[0] + "_" + String(event.eventInfo.longitude).split('.')[0], JSON.parse(chunk), 24 * 60 * 60 * 1000, function(error){
						if(error) console.log(error);

						doThings(JSON.parse(chunk));

						callback();
					});
				});
			});
			req.end();
		}else{
			doThings(data);
			callback();
		}
	});

	function doThings(w){
		var d = new Date(w.list[0].dt * 1000);
		event.weather = {
			datetime: new Date().toJSON().slice(0,19).replace('T', ' '),
			temp: w.list[0].main.temp,
			temp_min: w.list[0].main.temp_min,
			temp_max: w.list[0].main.temp_max,
			humidity: w.list[0].main.humidity,
			description: w.list[0].weather.description
		}
	}
}
function getEventArtist(artist, callback){
	memcached.get("api.spotify.com:" + artist.name, function(error, data){
		if(!data){
			var options = {
				host: "api.spotify.com",
				port: "443",
				path: "/v1/search?q=" + encodeURIComponent(artist.name) + "&type=artist",
				method: "get"
			};
			var req = https.request(options, function(res){
				var chunk = "";
				res.on("data", function(data){
					chunk += data;
				});
				res.on("end", function(){
					var a = JSON.parse(chunk);
					if(a.artists.items.length){
						var options = {
							host: "api.spotify.com",
							port: "443",
							path: "/v1/artists/" + a.artists.items[0].id + "/top-tracks?country=DE",
							method: "get"
						};
						var req = https.request(options, function(res){
							var chunk = "";
							res.on("data", function(data){
								chunk += data;
							});
							res.on("end", function(){
								memcached.set("api.spotify.com:" + artist.name, JSON.parse(chunk), 24 * 60 * 60 * 1000, function(error){
									if(error) console.log(error);

									doThings(JSON.parse(chunk))
								});
							});
						 });
						req.end();
					}else
						callback();
				});
			});
			req.end();
		}else{
			doThings(data);
		}
	});

	function doThings(a){
		artist.topTracks = new Array();
		for(var i = 0; i < a.tracks.length; i++){
			artist.topTracks.push({
				title: a.tracks[i].name,
				spotifyLink: a.tracks[i].href,
				albumCover: a.tracks[i].album.name
			});
		}
		callback();
	}
}